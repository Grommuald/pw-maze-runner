#include <vector>
#include <utility>
#include <thread>

#ifndef RUNNER_HPP
#define RUNNER_HPP

struct SDL_Renderer;
class Maze;

class Runner
{
public:
    enum Direction : unsigned char { Top, Left, Bottom, Right };

    Runner( SDL_Renderer*,
            Maze*,
            Runner*,
            const size_t&, const size_t&,
            const size_t&, const size_t&,
            const size_t&, const size_t&,
            const bool& = false );
    ~Runner();

    void Start();
    void Stop();
    bool IsFinished()           const       { return m_finished; };
    bool IsStarted()            const       { return m_started; };
    bool IsRoot()               const       { return m_isRoot; };

    uint64_t        GetID()     const       { return m_id; };
    Runner*         GetParent()             { return m_parent; };
    std::thread&    GetThread();

private:
    void threadEvaluation();

    Maze* m_mazePtr;
    SDL_Renderer* m_renderer;

    bool m_finished;
    bool m_started;
    bool m_isRoot;

    size_t m_parentX, m_parentY;
    size_t m_currentX, m_currentY;
    size_t m_targetX, m_targetY;

    Runner* m_parent;

    uint64_t m_id;
    std::thread m_thread;
    std::vector < std::unique_ptr<Runner> > m_childRunners;
};

#endif
