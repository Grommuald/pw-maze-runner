#include <vector>
#include <utility>
#include <memory>

#ifndef MAZE_HPP
#define MAZE_HPP

class Maze
{
public:
	Maze( const size_t& grid_width, const size_t& grid_height );
	~Maze();

	enum class State : unsigned char { Empty, Visited };
	enum Direction : unsigned char { Top, Left, Bottom, Right };

	struct Cell
	{
		State state { State::Empty };
		std::vector< Cell* > neighbours
		{
			nullptr,
			nullptr,
			nullptr,
			nullptr
		};
		size_t value { 0 };
	};

    void SetAsVisited( const size_t&, const size_t& );
	std::vector< Cell >& GetGrid();
    std::vector< Cell >& GetReversedGrid();
	std::pair< size_t, size_t> GetSize() const;

    void print();
    void printReversed();
private:
	void generate();
    void createReversedGrid();

	std::vector< Cell > m_cells;
	std::vector< Cell > m_reversedCells;

	size_t m_gridWidth;
	size_t m_gridHeight;
};

#endif
