#include <string>
#include <utility>
#include <memory>

#ifndef TILE_TEXTURE_HPP
#define TILE_TEXTURE_HPP

struct SDL_Texture;
struct SDL_Rect;
struct SDL_Renderer;

class TileTexture
{
public:
    TileTexture( std::pair< int, int > position, SDL_Rect* = nullptr );
    ~TileTexture();

    void render();

    static size_t GetWidth();
    static size_t GetHeight();

    static bool LoadGlobalTexture( SDL_Renderer*, const std::string& );
    static void FreeGlobalTexture();

private:
    std::shared_ptr< SDL_Rect > m_clip;
    std::pair< int, int > m_position;

    static SDL_Renderer* m_rendererHandle;
    static SDL_Texture* m_texture;
    static size_t m_width;
    static size_t m_height;
};
#endif
