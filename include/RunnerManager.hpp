#include <mutex>
#include <vector>
#include <map>
#include <thread>
#include <utility>
#include <condition_variable>
#include <string>

#ifndef RUNNER_MANAGER_HPP
#define RUNNER_MANAGER_HPP

struct SDL_Renderer;
class Runner;
class Maze;

class RunnerManager
{
public:
    static std::mutex m_gridMutex;
    static std::mutex m_switchMutex;
    static std::mutex m_visitMutex;
    static std::mutex m_randMutex1;
    static std::mutex m_randMutex2;
    static std::mutex m_rendererMutex;
    static std::mutex m_addToTraceMutex;
    static std::mutex m_childrenStatusMutex;
    static std::mutex m_addChildrenMutex;

    static std::mutex m_globalMutex;

    static void Start( const size_t&, const size_t& );
    static void End();
    static void MarkStartRunnerAsFinished();
    static void SetMaze( Maze* );
    static void SetRenderer( SDL_Renderer* );
    static void DrawCurrentTurn();
    static void RecreatePathFromStartToEnd( Runner* );
    static void AddToTrace( const uint64_t& id, std::pair< size_t, size_t> );

private:
    static bool m_startRunnerFinished;
    static Maze* m_mazePtr;
    static std::unique_ptr< Runner > m_startRunner;
    static std::vector< std::pair< size_t, size_t > > m_pathToDestination;
    static SDL_Renderer* m_renderer;
    static std::map< uint64_t, std::vector< std::pair< size_t, size_t > > > m_traces;
};
#endif
