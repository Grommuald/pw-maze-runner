#ifndef MAZE_APP_HPP
#define MAZE_APP_HPP

#include "Maze.hpp"
#include "TileTexture.hpp"

#include <string>

struct SDL_Renderer;
struct SDL_Window;
struct SDL_Surface;
struct SDL_Texture;

class Runner;

class MazeApp
{
public:
	MazeApp();
	~MazeApp();

	void Run();

private:
	static const size_t m_tileSize { 32 };
	static const size_t m_mazeWidth { 25 };
	static const size_t m_mazeHeight { 19 };

	static const size_t m_screenWidth { 800 };
	static const size_t m_screenHeight { 608 };
	static constexpr const char* m_windowCaption { "Maze Runners" };

	SDL_Renderer* m_renderer;
	SDL_Window* m_window;

	Maze m_maze;
    std::shared_ptr< Runner> m_startRunner;
	std::vector< TileTexture > m_tiles;

	void createMazeSurface();
	void draw();
	void update();
};
#endif
