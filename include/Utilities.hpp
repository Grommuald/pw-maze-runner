#include <random>

#ifndef UTILITIES_HPP
#define UTILITIES_HPP

class Utilities
{
public:
    static size_t RandomizeUInt( const size_t& a, const size_t& b );
    static double RandomizeReal( const double& a, const double& b );
private:
    static std::default_random_engine e;
};
#endif
