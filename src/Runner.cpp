#include "Runner.hpp"
#include "Utilities.hpp"
#include "Maze.hpp"
#include "RunnerManager.hpp"

#include <iostream>
#include <mutex>

Runner::Runner(
    SDL_Renderer* renderer,
    Maze* mazePtr,
    Runner* parent,
    const size_t& parentX,      const size_t& parentY,
    const size_t& startingX,    const size_t& startingY,
    const size_t& targetX,      const size_t& targetY,
    const bool& isRoot )
    :
    m_mazePtr( mazePtr ),
    m_renderer( renderer ),
    m_finished( false ),
    m_started( false ),
    m_isRoot( isRoot ),
    m_parentX( parentX ),
    m_parentY( parentY ),
    m_currentX( startingX ),
    m_currentY( startingY ),
    m_targetX( targetX ),
    m_targetY( targetY ),
    m_parent( parent ),
    m_thread {}
{
    m_id = reinterpret_cast< uint64_t >( this );
}

Runner::~Runner()
{

}

void Runner::Start()
{
    m_started = true;
    m_thread = std::thread( &Runner::threadEvaluation, this );
}

std::thread& Runner::GetThread()
{
    return m_thread;
}

void Runner::Stop()
{
    for ( auto& i : m_childRunners )
        if ( i->GetThread().joinable() )
            i->GetThread().join();
}

void Runner::threadEvaluation()
{
    const size_t numberOfNeighbours = 4;

    if ( m_parentX != m_currentX || m_parentY != m_currentY )
        RunnerManager::AddToTrace( m_id, std::make_pair( m_parentX, m_parentY ) );

    RunnerManager::AddToTrace( m_id, std::make_pair( m_currentX, m_currentY ) );
    while ( !m_finished )
    {
        RunnerManager::m_visitMutex.lock();
        m_mazePtr->SetAsVisited( m_currentX, m_currentY );
        RunnerManager::m_visitMutex.unlock();

        if ( m_currentX == m_targetX && m_currentY == m_targetY )
        {
            RunnerManager::RecreatePathFromStartToEnd( this );
            m_finished = true;
            break;
        }

        RunnerManager::m_gridMutex.lock();
        std::vector< Maze::Cell >& grid = m_mazePtr->GetReversedGrid();
        RunnerManager::m_gridMutex.unlock();

        std::vector< Direction > availableDirections;

        RunnerManager::m_switchMutex.lock();
        for ( size_t k = 0; k < numberOfNeighbours; ++k )
        {
            switch( static_cast< Direction >( k ) )
            {
                case Direction::Top:
                    if ( m_currentY > 0 )
                        if ( grid[ m_currentX + m_currentY * m_mazePtr->GetSize().first].neighbours[Direction::Top] != nullptr
                            && grid[ m_currentX + ( m_currentY - 1 ) * m_mazePtr->GetSize().first].state
                                == Maze::State::Empty )
                                {
                                    availableDirections.push_back( Direction::Top );
                                }
                break;

                case Direction::Left:
                    if ( m_currentX > 0 )
                        if ( grid[ m_currentX + m_currentY * m_mazePtr->GetSize().first].neighbours[Direction::Left] != nullptr
                            && grid[ m_currentX - 1 + m_currentY * m_mazePtr->GetSize().first].state
                                == Maze::State::Empty )
                                {
                                    availableDirections.push_back( Direction::Left );
                                }
                break;

                case Direction::Bottom:
                    if ( m_currentY < m_mazePtr->GetSize().second - 1 )
                        if ( grid[ m_currentX + m_currentY * m_mazePtr->GetSize().first].neighbours[Direction::Bottom] != nullptr
                            && grid[ m_currentX + ( m_currentY + 1 ) * m_mazePtr->GetSize().first].state
                                == Maze::State::Empty )
                                {
                                    availableDirections.push_back( Direction::Bottom );
                                }
                break;

                case Direction::Right:
                    if ( m_currentX < m_mazePtr->GetSize().first - 1 )
                        if ( grid[ m_currentX + m_currentY * m_mazePtr->GetSize().first].neighbours[Direction::Right] != nullptr
                            && grid[ m_currentX + 1 + m_currentY * m_mazePtr->GetSize().first].state
                                == Maze::State::Empty )
                                {
                                    availableDirections.push_back( Direction::Right );
                                }
                break;
            }
        }
        RunnerManager::m_switchMutex.unlock();

        if( availableDirections.size() == 0 )
        {
            m_finished = true;
            break;
        }
        else if( availableDirections.size() >= 1 )
        {
            RunnerManager::m_randMutex1.lock();

            size_t directionIndex = Utilities::RandomizeUInt( 0, availableDirections.size() - 1 );
            RunnerManager::m_randMutex1.unlock();

            size_t parentLastX = m_currentX;
            size_t parentLastY = m_currentY;

            Direction direction = availableDirections[ directionIndex ];
            switch( direction )
            {
                case Direction::Top:    --m_currentY; break;
                case Direction::Left:   --m_currentX; break;
                case Direction::Bottom: ++m_currentY; break;
                case Direction::Right:  ++m_currentX; break;
            }
            RunnerManager::AddToTrace( m_id, std::make_pair( m_currentX, m_currentY ) );

            if ( availableDirections.size() > 1 )
            {
                availableDirections.erase( availableDirections.begin() + directionIndex );

                {
                    std::lock_guard<std::mutex> lock( RunnerManager::m_addChildrenMutex );
                    for ( size_t i = 0; i < availableDirections.size(); ++i )
                    {
                        size_t childX = parentLastX;
                        size_t childY = parentLastY;

                        RunnerManager::m_randMutex2.lock();
                        directionIndex = Utilities::RandomizeUInt( 0, availableDirections.size() - 1 );
                        RunnerManager::m_randMutex2.unlock();

                        direction = availableDirections[ i ];

                        switch ( direction )
                        {
                            case Direction::Top:    --childY; break;
                            case Direction::Left:   --childX; break;
                            case Direction::Bottom: ++childY; break;
                            case Direction::Right:  ++childX; break;
                        }

                        m_childRunners.push_back( std::make_unique<Runner>(
                            m_renderer, m_mazePtr, this,
                            parentLastX, parentLastY,
                            childX, childY,
                            m_targetX, m_targetY ) );
                    }

                    for ( auto& i : m_childRunners )
                        if ( !i->IsStarted() )
                            i->Start();
                }
                while ( true )
                {
                    auto i = m_childRunners.cbegin();
                    while ( i != m_childRunners.cend() )
                    {
                        if ( !(*i)->IsFinished() )
                            break;
                        ++i;
                    }
                    if ( i == m_childRunners.cend() )
                        break;
                }
            }
        }
    }
    Stop();
    if ( m_isRoot )
    {
        RunnerManager::MarkStartRunnerAsFinished();
        std::cout << "startRunner has been ended.\n";
    }
}
