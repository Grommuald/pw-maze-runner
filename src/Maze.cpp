#include "Maze.hpp"
#include "Utilities.hpp"
#include <algorithm>
#include <iostream>

Maze::Maze( const size_t& grid_width, const size_t& grid_height )
	:
	m_gridWidth( grid_width ),
	m_gridHeight( grid_height )
{
	for ( size_t i = 0; i < m_gridWidth * m_gridHeight; ++i )
		m_cells.push_back( Cell() );

	// filling the m_cells.neighbours for each cell with it's neighbours pointers
	for ( size_t i = 0; i < m_gridHeight; ++i )
	{
		for ( size_t j = 0; j < m_gridWidth; ++j )
		{
			if ( j < m_gridWidth - 1 )
				m_cells[j + i*m_gridWidth].neighbours[Direction::Right] = &m_cells[j + 1 + i*m_gridWidth];
			if ( j > 0 )
				m_cells[j + i*m_gridWidth].neighbours[Direction::Left] = &m_cells[j - 1 + i*m_gridWidth];
			if ( i < m_gridHeight - 1 )
				m_cells[j + i*m_gridWidth].neighbours[Direction::Bottom] = &m_cells[j + ( i + 1 )*m_gridWidth];
			if ( i > 0 )
				m_cells[j + i*m_gridWidth].neighbours[Direction::Top] = &m_cells[j + ( i - 1 )*m_gridWidth];
		}
	}
	generate();
    createReversedGrid();
    printReversed();
	std::cout << "Maze.cpp: Maze: end\n";
}

Maze::~Maze()
{
}

void Maze::generate()
{
	using namespace std;

	auto append_to_vector = []( vector< Cell* >& dst, vector < Cell* >& src )
	{
		for ( auto i = src.begin(); i != src.end(); ++i )
			if ( *i != nullptr )
				dst.push_back( *i );
	};

	auto get_opposite_visited_cell = [] ( Maze::Cell* cell ) -> Cell*
	{
		for ( auto i : cell->neighbours )
			if ( i != nullptr && i->state == State::Visited )
				return i;
		return nullptr;
	};

	auto get_decimal_offset = []( const Maze::Cell& cell )
	{
		size_t offset = 0;
		size_t element_index = cell.neighbours.size() - 1;

		for ( const auto& i : cell.neighbours )
		{
			if ( i != nullptr )
				offset += 1 << element_index;

			--element_index;
		}
		return offset;
	};

	auto delete_wall = []( Maze::Cell* w1, Maze::Cell* w2 )
	{
		for ( auto& i : w1->neighbours )
		{
			if ( i == w2 )
			{
				i = nullptr;
				break;
			}
		}
		for ( auto& i : w2->neighbours )
		{
			if ( i == w1 )
			{
				i = nullptr;
				break;
			}
		}
	};

	size_t current_wall_index =
        Utilities::RandomizeUInt( 0, m_gridWidth - 1 ) + m_gridWidth * ( m_gridHeight - 1 );
	cout << current_wall_index << endl;

	Cell* current = &m_cells[current_wall_index];
	current->state = State::Visited;
	vector< Cell* > walls_to_visit;

	append_to_vector( walls_to_visit, current->neighbours );

	while ( !walls_to_visit.empty() )
	{
		current_wall_index = Utilities::RandomizeUInt( 0, walls_to_visit.size() - 1 );

		Cell* neighbour = walls_to_visit[ current_wall_index ];

		if ( neighbour->state == State::Empty )
		{
			Cell* opposite = get_opposite_visited_cell( neighbour );
			delete_wall ( opposite, neighbour );
			append_to_vector( walls_to_visit, neighbour->neighbours );
			neighbour->state = State::Visited;
		}
		for ( auto& i : walls_to_visit )
		{
			if ( i == neighbour )
			{
				std::swap( i, walls_to_visit.back() );
				walls_to_visit.pop_back();
				break;
			}
		}
	}

	for ( auto& i : m_cells )
		i.value = get_decimal_offset( i );

	std::cout << "Maze.cpp: generate: end\n";
}

void Maze::SetAsVisited( const size_t& x, const size_t& y)
{
    m_reversedCells[ x + y * m_gridWidth ].state = State::Visited;
}

void Maze::createReversedGrid()
{
    for ( size_t i = 0; i < m_gridWidth * m_gridHeight; ++i )
        m_reversedCells.push_back( Cell() );

    const size_t numberOfNeighbours = 4;

    for ( size_t i = 0; i < m_gridHeight; ++i )
    {
        for ( size_t j = 0; j < m_gridWidth; ++j )
        {
            for ( size_t k = 0; k < numberOfNeighbours; ++k )
                if ( m_cells[j + i*m_gridWidth].neighbours[k] == nullptr )
                    switch ( static_cast< Direction >( k ) )
                    {
                        case Direction::Top:
                            if ( i > 0 )
                                m_reversedCells[j + i*m_gridWidth].neighbours[Direction::Top] =
                                    &m_reversedCells[j + (i - 1)*m_gridWidth];
                            break;
                        case Direction::Left:
                            if ( j > 0 )
                                m_reversedCells[j + i*m_gridWidth].neighbours[Direction::Left] =
                                    &m_reversedCells[j - 1 + i * m_gridWidth];
                            break;
                        case Direction::Bottom:
                            if ( i < m_gridHeight - 1 )
                                m_reversedCells[j + i*m_gridWidth].neighbours[Direction::Bottom] =
                                    &m_reversedCells[j + (i + 1)*m_gridWidth];
                            break;
                        case Direction::Right:
                            if ( j < m_gridWidth - 1 )
                                m_reversedCells[j + i*m_gridWidth].neighbours[Direction::Right] =
                                    &m_reversedCells[j + 1 + i * m_gridWidth];
                            break;
                    }
        }
    }
}

void Maze::print()
{
	size_t index = 0;
	for ( const auto& i : m_cells )
	{
		std::cout << "\nCell: \nState: " << (i.state == State::Empty ? "Empty" : "Visited") << std::endl;
		for ( const auto& j : i.neighbours )
			std::cout << "Address: " << j << std::endl;
		std::cout << "Value: " << i.value << "\nIndex: " << index++ << "\n";
	}
}
void Maze::printReversed()
{
	size_t index = 0;
	for ( const auto& i : m_reversedCells )
	{
		std::cout << "\nCell: " << &i << "\nState: " << (i.state == State::Empty ? "Empty" : "Visited") << std::endl;
		for ( const auto& j : i.neighbours )
			std::cout << "Address: " << j << std::endl;
		std::cout << "Value: " << i.value << "\nIndex: " << index++ << "\n";
	}
}

std::vector<Maze::Cell>& Maze::GetGrid()
{
	return m_cells;
}

std::vector<Maze::Cell>& Maze::GetReversedGrid()
{
	return m_reversedCells;
}

std::pair< size_t, size_t > Maze::GetSize() const
{
	return std::make_pair( m_gridWidth, m_gridHeight );
}
