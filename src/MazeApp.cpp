#include "MazeApp.hpp"
#include "Runner.hpp"
#include "RunnerManager.hpp"
#include "TileTexture.hpp"
#ifdef _WIN32
	#include "SDL.h"
	#include "SDL_image.h"
    #include "SDL2_gfxPrimitives.h"
#else
	#include <SDL2/SDL.h>
	#include <SDL2/SDL_image.h>

#endif
#include <iostream>

const size_t MazeApp::m_tileSize;
const size_t MazeApp::m_mazeWidth;
const size_t MazeApp::m_mazeHeight;

const size_t MazeApp::m_screenWidth;
const size_t MazeApp::m_screenHeight;
constexpr const char* MazeApp::m_windowCaption;

MazeApp::MazeApp()
	:
	m_maze ( std::move( Maze( MazeApp::m_mazeWidth, MazeApp::m_mazeHeight ) ) )
{
	std::cout << "MazeApp.cpp: MazeApp: start\n";
	if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
		exit( 1 );

	if ( !( m_window = SDL_CreateWindow(
		MazeApp::m_windowCaption,
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		MazeApp::m_screenWidth,
		MazeApp::m_screenHeight,
		0
	) ) )
	{
		SDL_Quit();
		exit( 1 );
	}
	if (!( m_renderer = SDL_CreateRenderer( m_window, -1, 0 ))) {
			SDL_Quit();
			exit(1);
	}
	if ( !IMG_Init( IMG_INIT_PNG ) )
	{
		SDL_Quit();
		exit( 1 );
	}
	std::cout << "MazeApp.cpp: MazeApp: end\n";
	TileTexture::LoadGlobalTexture( m_renderer, "res/tiles.png" );
	createMazeSurface();

    RunnerManager::SetMaze( &m_maze );
    RunnerManager::SetRenderer( m_renderer );
    RunnerManager::Start( m_mazeWidth - 1, 0 );
}

MazeApp::~MazeApp()
{
    RunnerManager::End();

	if ( m_renderer )
		SDL_DestroyRenderer( m_renderer );
	if ( m_window )
		SDL_DestroyWindow( m_window );

	TileTexture::FreeGlobalTexture();

	IMG_Quit();
	SDL_Quit();
}

void MazeApp::Run()
{
	update();
}

void MazeApp::createMazeSurface()
{
	size_t index = 0;

	for ( const auto& i : m_maze.GetGrid() )
	{
		SDL_Rect src_clip;
		src_clip.x = i.value * m_tileSize;
		src_clip.y = 0;
		src_clip.w = m_tileSize;
		src_clip.h = m_tileSize;

		m_tiles.push_back( TileTexture(
			{ m_tileSize * ( index % m_mazeWidth ), m_tileSize * ( index / m_mazeWidth ) },
			&src_clip ) );

		++index;
	}
}

void MazeApp::draw()
{
	SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, SDL_ALPHA_OPAQUE );
	SDL_RenderClear( m_renderer );
	SDL_SetRenderDrawColor( m_renderer, 255, 255, 255, SDL_ALPHA_OPAQUE );

	for ( auto& i : m_tiles )
		i.render();

    RunnerManager::DrawCurrentTurn();
    SDL_RenderPresent( m_renderer );
}

void MazeApp::update()
{
	std::cout << "MazeApp.cpp: MazeApp::update: start\n";
	bool done = false;

	while ( !done )
	{
		SDL_Event event;
		while ( SDL_PollEvent( &event ) )
		{
			if ( event.type == SDL_QUIT )
				done = true;
		}
		draw();
	}
}
