#include "TileTexture.hpp"
#ifdef _WIN32
	#include "SDL.h"
	#include "SDL_image.h"
#else
	#include <SDL2/SDL.h>
	#include <SDL2/SDL_image.h>
#endif
#include <iostream>

SDL_Texture* TileTexture::m_texture = nullptr;
SDL_Renderer* TileTexture::m_rendererHandle = nullptr;

size_t TileTexture::m_width;
size_t TileTexture::m_height;

size_t TileTexture::GetWidth() { return TileTexture::m_width; }
size_t TileTexture::GetHeight() { return TileTexture::m_height; }

TileTexture::TileTexture( std::pair< int, int > position, SDL_Rect* clip )
{
    m_position = position;
    m_clip = std::make_shared< SDL_Rect >( *clip );
}

TileTexture::~TileTexture()
{

}

bool TileTexture::LoadGlobalTexture( SDL_Renderer* renderer, const std::string& path )
{
    m_rendererHandle = renderer;

    //The final texture
  	SDL_Texture* newTexture = NULL;
  	//Load image at specified path
  	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
  	if( loadedSurface == NULL )
  	{
  		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
  	}
  	else
  	{
  		//Color key image
  		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

  		//Create texture from surface pixels
      newTexture = SDL_CreateTextureFromSurface( m_rendererHandle, loadedSurface );
  		if( newTexture == NULL )
  		{
  			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
  		}
  		else
  		{
  			//Get image dimensions
  			TileTexture::m_width = loadedSurface->w;
  			TileTexture::m_height = loadedSurface->h;
  		}

  		//Get rid of old loaded surface
  		SDL_FreeSurface( loadedSurface );
  	}

  	//Return success
  	m_texture = newTexture;
  	return m_texture != NULL;
}

void TileTexture::FreeGlobalTexture()
{
    if ( TileTexture::m_texture )
    {
        SDL_DestroyTexture( TileTexture::m_texture );
        TileTexture::m_texture = nullptr;
        TileTexture::m_width = TileTexture::m_height = 0;
    }
}

void TileTexture::render()
{
    SDL_Rect render_quad = {
      m_position.first,
      m_position.second,
      static_cast< int >( TileTexture::m_width ),
      static_cast< int >( TileTexture::m_height )
    };

    if ( m_clip != nullptr )
    {
        render_quad.w = m_clip->w;
        render_quad.h = m_clip->h;
    }
    SDL_RenderCopy(
      TileTexture::m_rendererHandle,
      TileTexture::m_texture,
      m_clip.get(),
      &render_quad );
}
