#include "RunnerManager.hpp"
#include "Runner.hpp"
#include "Maze.hpp"

#ifdef _WIN32
	#include "SDL.h"
	#include "SDL2_gfxPrimitives.h"
#else
	#include <SDL2/SDL.h>
	#include <SDL2/SDL2_gfxPrimitives.h>
#endif

#include <iostream>
#include <iomanip>

bool RunnerManager::m_startRunnerFinished = false;
Maze* RunnerManager::m_mazePtr = nullptr;

std::unique_ptr< Runner >  RunnerManager::m_startRunner;

SDL_Renderer* RunnerManager::m_renderer = nullptr;
std::map< uint64_t, std::vector< std::pair< size_t, size_t > > > RunnerManager::m_traces;
std::vector < std::pair< size_t, size_t > > RunnerManager::m_pathToDestination;

std::mutex RunnerManager::m_globalMutex;
std::mutex RunnerManager::m_gridMutex;
std::mutex RunnerManager::m_switchMutex;
std::mutex RunnerManager::m_visitMutex;
std::mutex RunnerManager::m_randMutex1;
std::mutex RunnerManager::m_randMutex2;
std::mutex RunnerManager::m_rendererMutex;
std::mutex RunnerManager::m_addToTraceMutex;
std::mutex RunnerManager::m_childrenStatusMutex;
std::mutex RunnerManager::m_addChildrenMutex;

void RunnerManager::MarkStartRunnerAsFinished()
{
    m_startRunnerFinished = true;
}

void RunnerManager::Start( const size_t& targetX, const size_t& targetY )
{
    m_startRunner = std::move(
        std::make_unique< Runner >(
            m_renderer,
            m_mazePtr,
            nullptr,
            0, m_mazePtr->GetSize().second - 1,
            0, m_mazePtr->GetSize().second - 1,
            targetX, targetY, true )
    );
    m_startRunner->Start();
}

void RunnerManager::End()
{
    m_startRunner->GetThread().join();
    std::cout << "RunnerManager finished!\n";
}

void RunnerManager::SetMaze( Maze* maze )
{
    m_mazePtr = maze;
}

void RunnerManager::SetRenderer( SDL_Renderer* renderer )
{
    m_renderer = renderer;
}

void RunnerManager::RecreatePathFromStartToEnd( Runner* lastRunner )
{
    std::vector< uint64_t > finalPathNodes;
    Runner* parentNode = lastRunner;
    while( parentNode != nullptr )
    {
        finalPathNodes.push_back( parentNode->GetID() );
        parentNode = parentNode->GetParent();
    }

    for ( auto i = finalPathNodes.rbegin(); i != finalPathNodes.rend(); ++i )
    {
        std::pair< size_t, size_t > nextMatching =
            i+1 < finalPathNodes.rend() ? m_traces[*(i+1)][0] : m_traces[*i][0];

        for ( auto j : m_traces[*i] )
        {
            if ( i != finalPathNodes.rend() - 1
                && j.first == nextMatching.first && j.second == nextMatching.second )
                break;

            m_pathToDestination.push_back( j );
        }
    }
}

void RunnerManager::AddToTrace( const uint64_t& id, std::pair< size_t, size_t> coord )
{
    std::lock_guard<std::mutex> lock(m_addToTraceMutex);
    //std::cout << "Thread: " << std::this_thread::get_id() << " - pushing value " << coord.first << " " <<coord.second << std::endl;

    m_traces[id].push_back( coord );

    /*
    std::cout << "Current threads table:\n";
    for ( auto i = m_traces.cbegin(); i != m_traces.cend(); ++i )
    {
        std::cout << i->first << std::endl;
        for ( auto j : (*i).second )
            std::cout << std::setw( 10 ) << j.first << " " << j.second << std::endl;
    }
    */
}

void RunnerManager::DrawCurrentTurn()
{
    uint16_t r = 255;
    uint16_t g = 0;
    uint16_t b = 0;

    for ( auto i = m_traces.rbegin(); i != m_traces.rend(); ++i )
    {
        for ( auto j = (*i).second.begin(); j != (*i).second.end() - 1; ++j )
        {
            lineRGBA(
                m_renderer,
                (*j).first * 32 + 16,
                (*j).second * 32 + 16,
                (*( j+1 )).first * 32 + 16,
                (*( j+1 )).second * 32 + 16,
                r, g, b, 255
            );
        }
        r -= 10;
        g += 20;
        b += 20;
    }

    if ( m_startRunnerFinished )
    {
        for ( auto i = m_pathToDestination.cbegin(); i != m_pathToDestination.cend() - 1; ++i )
        {
            thickLineRGBA(
                m_renderer,
                (*i).first * 32 + 16,
                (*i).second * 32 + 16,
                (*(i+1)).first * 32 + 16,
                (*(i+1)).second * 32 + 16,
                2,
                0, 0, 255, 255
            );
        }
    }
    std::this_thread::sleep_for( std::chrono::milliseconds( 200 ) );
}
