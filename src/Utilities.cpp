#include "Utilities.hpp"
#include <random>

std::default_random_engine Utilities::e( std::random_device{}() );

size_t Utilities::RandomizeUInt( const size_t& a, const size_t& b )
{
    std::uniform_int_distribution< size_t > u( a, b );
    return u( e );
}

double Utilities::RandomizeReal( const double& a, const double& b )
{
    std::uniform_real_distribution< double > u( a, b );
    return u( e );
}
