OUT = bin/labyrinth
CC = g++
IFLAGS = -Iinclude
CFLAGS = -Wall -std=c++14
LDFLAGS = -lGL -lSDL2 -lSDL2_image -lSDL2_gfx -lpthread
SOURCES = src/*

OBJECTS = $(SOURCES:.c=.o)

all:
	mkdir bin
	$(CC) $(IFLAGS) $(CFLAGS) -o $(OUT) $(SOURCES) $(LDFLAGS)
clean:
	rm -rf bin
